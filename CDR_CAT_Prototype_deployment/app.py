"""
    Contains the Flask Instance.
    Run this script !!
"""

from flask import Flask, render_template, flash
from selenium import webdriver
from selenium.webdriver import ChromeOptions
import trafilatura
from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, SubmitField
from wtforms.validators import DataRequired
import utils
from prediction import predict_url, predict_multilabel
import rate_cdr_dim




app = Flask(__name__)
# secret key
app.config['SECRET_KEY'] = "cdrcatprototype"

stringfield_error_message = None

label_descriptions = {
    "Access" : "Access",
    "Awareness": "Training and Awareness",
    "Transparency": "Information and Transparency",
    "Financial": "Financial Interests",
    "Security": "Product Safety and Liability",
    "Privacy": "Privacy and Data Protection",
    "Compensation": "Dispute Resolution and Compensation",
    "Participation": "Participation"
}

class Form(FlaskForm):
    url = StringField("Enter URL", validators=[DataRequired()], render_kw={"placeholder": "Enter URL"})
    url_model = SelectField("Model for URL Classification", validators=[DataRequired()],
                            choices= [(0, "Naive Bayes"), (1, "Random Forest"), (2, "BERT and Random Forest")])
    dim_model = SelectField("Model for Prediction of CDR Dimensions", validators=[DataRequired()],
                            choices= [(0, "Naive Bayes"), (1, "Random Forest"), (2, "BERT and Random Forest")])
    submit = SubmitField("Submit")


def extract_text(link):
    global stringfield_error_message
    try:
        options = ChromeOptions()
        options.headless = True
        driver = webdriver.Chrome(executable_path="drivers/chromedriver", options=options)
        driver.get(link)
        main_content = trafilatura.extract(driver.page_source, 
                                    include_comments=False,
                                    include_tables=False, 
                                    no_fallback=True,)
        return main_content

    except Exception as e:
        stringfield_error_message = "URL could not be accessed !"
        

@app.route("/")
@app.route("/index")
def hello():
    form = Form()
    return render_template("index.html", form=form)

@app.route("/predict", methods = ["POST"])
def submit():
    global stringfield_error_message
    url_prediction, dim_prediction, dim_rating = None, None, None
    form = Form()
    link = None
    if form.validate_on_submit():
        link = form.url.data
        url_model = int(form.url_model.data)
        dim_model = int(form.dim_model.data)
        text = extract_text(link) 

        # If URL can not be accessed, flash message appears on index page
        if stringfield_error_message != None:
            flash(stringfield_error_message)
            return render_template("index.html", form=form)

        language = utils.predict_lang(text)  
               
        if (language == 'EN' or language == 'DE'):
            # Initialization of URL classification models
            predict_url.initalize_model(language, url_model)
            # Initalization of multilabel CDT-Dimensions classifier
            predict_multilabel.initalize_model(language, dim_model)

            # url classification
            if url_model == 0 or url_model == 1:
                url_prediction = predict_url.predict_text(text)
            if url_model == 2:
                url_prediction = predict_url.predict_text_bert(text, language)
            
            # multilabel(CDR-DIM) classification
            if dim_model == 0 or dim_model == 1:
                dim_prediction = predict_multilabel.predict_text(text)
                dim_rating = rate_cdr_dim.rate_cdr_dim(text, language)            
            if dim_model == 2:
                dim_prediction = predict_multilabel.predict_text_bert(text, language)
                dim_rating = rate_cdr_dim.rate_cdr_dim(text, language)
    
        return render_template("predict.html", url_prediction = url_prediction, 
                                            dim_prediction=dim_prediction, 
                                            dim_rating=dim_rating,
                                            description=label_descriptions,
                                            url=link,
                                            text=text,
                                            language=language)   



# invalid URL
@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404   

# Interval server error
@app.errorhandler(500)
def page_not_found(e):
    return render_template("500.html"), 500

if __name__ == "__main__":
    app.run(debug=True)
# host='0.0.0.0', port=8080