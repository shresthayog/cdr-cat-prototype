from cdr import access, awareness_training, dispute_resolution_compensation
from cdr import financial_interests, information_transparncy, participation
from cdr import privacy_data_protection, product_safety_liability


def rate_cdr_dim(text, language):
    if language == 'DE' or language == 'EN':
        cdr_dim_ratings = {
        "Access" : access.rating(text, language),
        "Awareness": awareness_training.rating(text, language),
        "Transparency": information_transparncy.rating(text, language),
        "Financial": financial_interests.rating(text, language),
        "Security": product_safety_liability.rating(text, language),
        "Privacy": privacy_data_protection.rating(text, language),
        "Compensation": dispute_resolution_compensation.rating(text, language),
        "Participation": participation.rating(text, language)
        }
        return cdr_dim_ratings
    else:
        return None