from cdr import rating_scheme

def access_de(text):
    """"
        - computes the score for  CDR dimension 'Access'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'zugang' in text:
        n_words += 1
    if 'server' in text:
        n_words += 1
    if 'firewall' in text:
        n_words += 1


    score = n_words / 3.0
    return score

def access_en(text):
    """"
        - computes the score for  CDR dimension 'Access'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'access' in text:
        n_words += 1
    if 'server' in text:
        n_words += 1
    if 'firewall' in text:
        n_words += 1

    score = n_words / 3.0
    return score

def rating(text, language):
    if language == 'DE':
        score = access_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = access_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None