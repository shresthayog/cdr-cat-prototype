def rate(score):
    if score >= 0.0 and score < 0.1:
        rating = 1
    elif score >= 0.1 and score < 0.2:
        rating = 2
    elif score >= 0.2 and score < 0.3:
        rating = 3
    elif score >= 0.3 and score < 0.4:
        rating = 4
    elif score >= 0.4 and score < 0.5:
        rating = 5
    elif score >= 0.5 and score < 0.6:
        rating = 6
    elif score >= 0.6 and score < 0.7:
        rating = 7
    elif score >= 0.7 and score < 0.8:
        rating = 8
    elif score >= 0.8 and score < 0.9:
        rating = 9
    else:
        rating = 10
    return rating
    