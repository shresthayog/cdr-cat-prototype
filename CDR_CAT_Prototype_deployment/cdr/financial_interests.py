from cdr import rating_scheme

def score_financial_interest_de(text):
    """"
        - computes the score for  CDR dimension 'Financial interests'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'erstatt' in text or 'vergüt' in text:
        n_words += 1
    if 'widerruf' in text:
        n_words += 1
    if 'trial' in text or 'test' in text:
        n_words += 1
    if 'kredit' in text:
        n_words += 1
    if 'bonus' in text:
        n_words += 1
    if 'ratenzahlung' in text:
        n_words += 1

    score = n_words / 6.0
    return score


def score_financial_interest_en(text):
    """"
        - computes the score for  CDR dimension 'Financial interests'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'refund' in text or 'compensat' in text:
        n_words += 1
    if 'withdraw' in text:
        n_words += 1
    if 'trial' in text or 'test' in text:
        n_words += 1
    if 'credit' in text:
        n_words += 1
    if 'bonus' in text:
        n_words += 1
    if 'installment' in text:
        n_words += 1

    score = n_words / 6.0
    return score

def rating(text, language):
    if language == 'DE':
        score = score_financial_interest_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_financial_interest_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None