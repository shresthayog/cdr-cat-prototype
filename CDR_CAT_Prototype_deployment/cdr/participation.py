from cdr import rating_scheme

def score_participation_de(text):
    """"
        - computes the score for  CDR dimension 'Participation'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'newsletter' in text:
        n_words += 1
    if 'umfrage' in text:
        n_words += 1

    score = n_words / 2.0
    return score

def score_participation_en(text):
    """"
        - computes the score for  CDR dimension 'Participation'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'newsletter' in text:
        n_words += 1
    if 'survey' in text:
        n_words += 1

    score = n_words / 2.0
    return score

def rating(text, language):
    if language == 'DE':
        score = score_participation_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_participation_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None