from cdr import rating_scheme

def score_product_safety_liability_de(text):
    """
        - computes the score for  CDR dimension 'Product Safety and Liability'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'zertifikat' in text:
        n_words += 1
    if 'haftung' in text or ' haften' in text:
        n_words += 1
    if 'erstatt' in text or 'vergüt' in text:
        n_words += 1
    if 'widerruf' in text:
        n_words += 1
    if 'garantie' in text or 'gewährleist' in text or 'warranty' in text:
        n_words += 1
    score = n_words / 5.0
    return score

def score_product_safety_liability_en(text):
    """
        - computes the score for  CDR dimension 'Product Safety and Liability'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'certificate' in text:
        n_words += 1
    if 'liability' in text or 'liable' in text:
        n_words += 1
    if 'refund' in text or 'compensat' in text:
        n_words += 1
    if 'withdraw' in text:
        n_words += 1
    if 'guarantee' in text or 'warranty' in text:
        n_words += 1
    score = n_words / 5.0
    return score

def rating(text, language):
    if language == 'DE':
        score = score_product_safety_liability_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_product_safety_liability_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None

