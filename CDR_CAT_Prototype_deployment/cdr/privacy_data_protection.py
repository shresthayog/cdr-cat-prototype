from cdr import rating_scheme

def score_privacy_data_protection_de(text):
    """
        - computes the score for  CDR dimension 'Privacy and Data Protection'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()

    if 'datenschutz' in text or 'datensicherheit' in text or 'privateschutz' in text:
        n_words += 1
    if 'dsgvo' in text :
        n_words += 1
    if 'cookie' in text:
        n_words += 1
    if 'ip-adress' in text or 'ip address' in text:
        n_words += 1
    if 'track' in text:
        n_words += 1
    if 'lösch' in text: 
        n_words += 1
    if 'speicher' in text:
        n_words += 1
    if 'personenbezogene daten' in text:
        n_words += 1
    if 'datenweitergabe' in text or 'datentransfer' in text or 'weitergabe' in text:
        n_words += 1

    score = n_words / 9.0
    return score

def score_privacy_data_protection_en(text):
    """
        - computes the score for  CDR dimension 'Privacy and Data Protection'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()

    if 'data protection' in text or 'data security' in text or 'privacy' in text:
        n_words += 1
    if 'dsgvo' in text :
        n_words += 1
    if 'cookie' in text:
        n_words += 1
    if 'ip-adress' in text or 'ip address' in text:
        n_words += 1
    if 'track' in text:
        n_words += 1
    if 'delete' in text: 
        n_words += 1
    if 'personal data' in text:
        n_words += 1
    if 'data transfer' in text or 'data forward' in text or 'forward' in text or 'transfer' in text:
        n_words += 1

    score = n_words / 8.0
    return score

def rating(text, language):
    if language == 'DE':
        score = score_privacy_data_protection_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_privacy_data_protection_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None


