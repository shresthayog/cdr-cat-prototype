from cdr import rating_scheme

def score_information_transparency_de(text):
    """
        - computes the score for  CDR dimension 'Information and Transparency'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()

    if 'künstliche intelligenz' in text or 'KI' in text:
        n_words += 1
    if 'cookie' in text:
        n_words += 1
    if 'ip-adress' in text or 'ip address' in text:
        n_words += 1
    if 'track' in text:
        n_words += 1
    if 'lösch' in text: 
        n_words += 1
    if 'speicher' in text:
        n_words += 1
    if 'personenbezogene daten' in text:
        n_words += 1
    if 'geschäftsmodell' in text:
        n_words += 1
    if 'datenweitergabe' in text or 'datentransfer' in text or 'weitergabe' in text:
        n_words += 1
    if 'third party' in text or 'dritt' in text:
        n_words += 1
    if 'medien' in text or 'social media' in text or 'soziale medien' in text:
        n_words += 1
    if 'facebook' in text or 'twitter' in text or 'youtube' in text:
        n_words += 1
    if 'datenverarbeitung' in text or 'verarbeit' in text:
        n_words += 1

    score = n_words / 13.0
    return score


def score_information_transparency_en(text):
    """
        - computes the score for  CDR dimension 'Information and Transparency'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()

    if 'artificial intelligence' in text or 'AI' in text:
        n_words += 1
    if 'cookie' in text:
        n_words += 1
    if 'ip-adress' in text or 'ip address' in text:
        n_words += 1
    if 'track' in text:
        n_words += 1
    if 'delete' in text: 
        n_words += 1
    if 'personal data' in text:
        n_words += 1
    if 'business model' in text:
        n_words += 1
    if 'data transfer' in text or 'data forward' in text or 'forward' in text or 'transfer' in text:
        n_words += 1
    if 'third party' in text or 'third' in text:
        n_words += 1
    if 'media' in text or 'social media' in text:
        n_words += 1
    if 'facebook' in text or 'twitter' in text or 'youtube' in text:
        n_words += 1
    if 'data processing' in text or 'process' in text:
        n_words += 1

    score = n_words / 12.0
    return score


def rating(text, language):
    if language == 'DE':
        score = score_information_transparency_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_information_transparency_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None


