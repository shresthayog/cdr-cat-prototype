from cdr import rating_scheme

def dr_compensation_de(text):
    """
        - computes the score for  CDR dimension 'Dispute Resolution and Compensation'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()

    if 'telephone' in text or 'tel' in text or 'phone' in text or 'fon' in text or 'telefon' in text:
        n_words += 1
    if 'email' in text or 'e-mail' in text or 'mail' in text:
        n_words += 1
    if 'fax' in text or 'telefax' in text:
        n_words += 1
    if 'registergericht' in text or 'register-gericht' in text or 'amtgericht' in text or  \
        'handelsregister' in text:
        n_words += 1
    if 'registernummer' in text or 'hrb' in text or 'gnr' in text or 'hra' in text:
        n_words += 1
    if 'ust-id-nr' in text or 'umsatzsteuer-identifikationsnummer' in text or 'umsatzsteuer-id' in text \
        or 'ust-idnr' in text or 'ust-identifikations-nr' or 'betriebsnummer' in text or 'steuer' in text:
        n_words += 1
    if 'geschäftsführer' in text or 'vorsitzend' in text or 'aufsichtsrat' in text or 'vertret' in text \
        or 'vorstandsmitglied' in text or 'verantwortlich' in text or 'geschäftsführung' in text:
        n_words += 1
    if 'streitbeilegung' in text:
        n_words += 1

    score = n_words / 8.0
    return score

def dr_compensation_en(text):
    """
        - computes the score for  CDR dimension 'Dispute Resolution and Compensation'
        based in buzzwords contained in the text 
    """
    n_words = 0
    text = text.lower()
    if 'telephone' in text or 'tel' in text or 'phone' in text or 'fon' in text or 'telefon' in text:
        n_words += 1
    if 'email' in text or 'e-mail' in text or 'mail' in text:
        n_words += 1
    if 'fax' in text or 'telefax' in text:
        n_words += 1
    if 'commericial registry' in text or 'hrb' in text or 'gnr' in text or 'hra' in text \
        or 'court of registry' in text or 'amtgericht' in text or 'registration' in text \
        or 'court' in text or 'register' in text:
        n_words += 1
    if 'vat id no' in text or 'vat id nr' in text or 'vat id' in text or 'vat i.d.' in text or 'vat' in text \
        or 'tax' in text:
        n_words += 1
    if 'supervisory board' in text or 'executive board' in text or 'managing director' in text \
        or 'management board' in text or 'chairperson' in text:
        n_words += 1
    if 'disclaimer' in text:
        n_words += 1
    if 'regulatory authorities' in text or 'regulatory' in text:
        n_words += 1

    score = n_words / 8.0
    return score

def rating(text, language):
    if language == 'DE':
        score = dr_compensation_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = dr_compensation_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None

