from cdr import rating_scheme

def score_awareness_training_de(text):
    """
        - computes the score for  CDR dimension 'Awareness and Training'
        based in buzzwords contained in the text
    """
    n_words = 0
    text = text.lower()

    if 'klima' in text or 'natur' in text  or 'umwelt' in text:
        n_words += 1
    if 'ökolog' in text or 'ökosystem' in text:
        n_words += 1
    if 'erneurbar' in text or 'wiederverwend' in text or 'recycl' in text:
        n_words += 1
    if 'gesund' in text or 'sicher' in text:
        n_words += 1
    if 'abfall' in text or 'entsorg' in text:
        n_words += 1
    if 'effizien' in text or 'produktiv' in text or 'effektiv' in text or 'resourcenschutz' in text:
        n_words += 1
    if 'nachhaltig' in text:
        n_words += 1
    if 'energie' in text or 'grün' in text:
        n_words += 1
    if 'transpar' in text:
        n_words += 1
    if 'ethik' in text or 'ethisch' in text:
        n_words += 1
    if 'meschenrecht' in text or 'gerechtig' in text:
        n_words += 1
    if 'wirtschaft' in text or 'gesellschaft' in text or 'gemeinschaft' in text or 'ökonom' in text:
        n_words += 1
    if 'förder' in text or 'motiva' in text or 'motivi' in text:
        n_words += 1
    if 'praktikum' in text or 'stipend' in text:
        n_words += 1
    if 'training' in text or 'trainieren':
        n_words += 1

    score = n_words / 15.0
    return score


def score_awareness_training_en(text):
    """
        - searches for buzzwords that are often seen for the dimension
        'awareness and training'
        - rating based on relevant words found
    """
    n_words = 0
    text = text.lower()

    if 'climate' in text or 'nature' in text  or 'enviroment' in text:
        n_words += 1
    if 'ecolog' in text or 'ecosystem' in text:
        n_words += 1
    if 'renewabl' in text or 'reus' in text or 'recycl' in text:
        n_words += 1
    if 'health' in text or 'safe' in text or 'safety' in text:
        n_words += 1
    if 'waste' in text or 'disposal' in text:
        n_words += 1
    if 'efficient' in text or 'productive' in text or 'effective' in text or 'protect' in text:
        n_words += 1
    if 'sustain' in text:
        n_words += 1
    if 'energy' in text or 'green' in text:
        n_words += 1
    if 'transpar' in text:
        n_words += 1
    if 'ethic' in text:
        n_words += 1
    if 'human right' in text or 'justice' in text or 'fair' in text:
        n_words += 1
    if 'economy' in text or 'society' in text or 'community' in text:
        n_words += 1
    if 'promot' in text or 'motivat' in text or 'motivi' in text:
        n_words += 1
    if 'intership' in text or 'scholarship' in text or 'apprentice' in text:
        n_words += 1
    if 'training' in text or 'train':
        n_words += 1

    score = n_words / 15.0
    return score

def rating(text, language):
    if language == 'DE':
        score = score_awareness_training_de(text)
        rating = rating_scheme.rate(score)
        return rating
    elif language == 'EN':
        score = score_awareness_training_en(text)
        rating = rating_scheme.rate(score)
        return rating
    else:
        return None
    

