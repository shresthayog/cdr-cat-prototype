"""
    Project: CDR-CAT-Prototype
    Prediction of URL category
"""
from transformers import DistilBertTokenizer
from transformers import DistilBertModel
import torch
import numpy as np
import utils
import dill
import spacy

model, vectorizer, encoder, nlp_model = None, None, None, None

def initalize_model(language, url_model_type):
    global model, vectorizer, encoder, nlp_model
    with open("./models/label_encoder.sav", "rb") as encoder:
        encoder = dill.load(encoder)
    if url_model_type == 0 and language == 'DE':
        with open("./models/vectorizer_url_de.sav", "rb") as vectorizer:
            vectorizer = dill.load(vectorizer)
        with open("./models/naive_bayes_model_de.sav", "rb") as model:
            model = dill.load(model)
        nlp_model = spacy.load('./models/de_core_news_md/de_core_news_md-3.2.0')
    elif url_model_type == 0 and language == 'EN':
        with open("./models/naive_bayes_model_en.sav", "rb") as model:
            model = dill.load(model)
        with open("./models/vectorizer_url_en.sav", "rb") as vectorizer:
            vectorizer = dill.load(vectorizer)
        nlp_model = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
    elif url_model_type == 1 and language == 'DE':
        with open("./models/random_forest_model_de.sav", "rb") as model:
            model = dill.load(model)
        with open("./models/vectorizer_url_de.sav", "rb") as vectorizer:
            vectorizer = dill.load(vectorizer)
        nlp_model = spacy.load('./models/de_core_news_md/de_core_news_md-3.2.0')
    elif url_model_type == 1 and language == 'EN':
        with open("./models/random_forest_model_en.sav", "rb") as model:
            model = dill.load(model)
        with open("./models/vectorizer_url_en.sav", "rb") as vectorizer:
            vectorizer = dill.load(vectorizer)
        nlp_model = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
    elif url_model_type == 2 and language == 'DE':
        with open("./models/bert_rf_model_de.sav", "rb") as model:
            model = dill.load(model)
        nlp_model = spacy.load('./models/de_core_news_md/de_core_news_md-3.2.0') 
    elif url_model_type == 2 and language == 'EN':
        with open("./models/bert_rf_model_en.sav", "rb") as model:
            model = dill.load(model)
        nlp_model = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")



def generate_features_bert(language, text_features):
    """
    - Generates the representation of 'text_features'
    """
    NUM_CHUNK = 4
    if language == "EN":
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = DistilBertModel.from_pretrained('distilbert-base-uncased')
    else:
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-german-cased')
        model = DistilBertModel.from_pretrained('distilbert-base-german-cased')
    tokenized = tokenizer.encode(text_features,
                                add_special_tokens=True,
                                truncation=False,)
    tokenized = torch.tensor(tokenized)
    # split tensor in max length of 512
    input_id_chunks = tokenized.split(512)
    mask_chunks = tokenized.split(512)
    # create zero tensors
    input_ids = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
    attention_mask = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
    # number of splits
    len_chunk = len(input_id_chunks)

    if len_chunk > NUM_CHUNK:
        for i in range(NUM_CHUNK):
            input_ids[i] = input_id_chunks[i]
            attention_mask[i] = mask_chunks[i]
    else:
        for i in range(len_chunk):
            pad_len = 512 - input_id_chunks[i].shape[0]
            if pad_len > 0:
                input_ids[i] = torch.cat([input_id_chunks[i], torch.Tensor([0] * pad_len)])
                attention_mask[i] = torch.cat([mask_chunks[i], torch.Tensor([0] * pad_len)])
            else:
                input_ids[i] = input_id_chunks[i]
                attention_mask[i] = mask_chunks[i]
    # feature generation, reshaping, substituting 'nan' with 0
    with torch.no_grad():
        last_hidden_states = model(input_ids, attention_mask=attention_mask)
    features = torch.reshape(last_hidden_states[0][:,0,:], (-1,)).numpy()
    where_are_nans = np.isnan(features)
    features[where_are_nans] = 0
    return features


def predict_text_bert(text, language):
    """
    - Predict the CDR Dimension of a single text
    """
    processed_text = utils.process_text_bert(text)
    bert_out = generate_features_bert(language, processed_text)
    model_out = model.predict(bert_out[np.newaxis,:])
    pred_class = encoder.inverse_transform(model_out)[0]
    return pred_class


def predict_text(text):
    """
    Categorize a single text
    """
    processed_text = utils.process_text(nlp_model, text)
    x = vectorizer.transform([processed_text]).toarray()
    y = model.predict(x)
    pred_class = encoder.inverse_transform(y)[0]
    return pred_class

    


