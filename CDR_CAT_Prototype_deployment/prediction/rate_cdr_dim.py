import re
import numpy as np
from sklearn.metrics import accuracy_score


buzzwords_en = {
    "Access":           ["access", "server"],
    "Awareness":        ["sustain", "enviroment", "climate", "emission", "natur", "empower", "effectiv"
                        "efficien", "ecolog", "eco-friendly", "ecosystem", "renewabl", "recycl", "reus",
                        "safe", "human rights", "innovat", "community", "conserv", "organic", "society"
                        "preserv", "econom", "noise", "aware", "carbon footprint", "ethic", "quality",
                        "biodivers", "train", "promot", "vocational", "encourag", "support", "apprentice",
                        "opportunity", "motivat", "internship", "scholarship", "skill", "productiv", "animal"
                        "health", "safe", "values", "culture", "fair", "profession", "commit", "transpar", 
                        "green"],
    "Transparency": 	["artificial intellig", "AI", "stor", "transmi", "transfer", "shar", "third part", 
                        "cookie", "business modell", "security", "right", "certificat", "processing", 
                        "social media", "youtube", "twitter", "facebook"],
    "Financial":        ["price", "pay", "trial", "financ", "bill", "credit", "bonus"],
    "Security":         ["certificat", "liability", "guarantee", "warrant", "refund", "withdraw", "compensation"],
    "Privacy":          ["privacy", "data protection", "dsgvo", "cookie", "ip address", "track", "personal", "delete", 
                        "security", "stor"],
    "Compensation":     ["compens", "contact", "phone", "mail", "fax", "authorit", "regist", "legal","board", 
                        "vat ", "tax"],
    "Participation":   ["participat", "newsletter", "subscri", "survey", "track", "stakeholder", 
                        "research", "feedback"],     
}


buzzwords_de = {
    "Access":           ["zugang", "server"],
    "Awareness":        ["nachhaltig", "qualität", "klima", "ausstoß", "natur", "effektiv", "effizien",
                        "ökolog", "umwelt", "ökosystem", "erneurbar", "recycl", "wiederverwend",
                        "sicher", "menschenrecht", "innovat", "gemeinschaft", "gesellschaft", "erhalt",
                        "wirtschaft", "Lärm", "ethik", "biodiversität", "förder", "gelegenheit", "motiv",
                        "praktikum", "stipend", "Fähigkeit", "produktiv", "gesund", "sicher", "wert",
                        "kultur", "gerechtig", "professionell", "transpare", "vergüt", "herausforderung",
                        "abfall", "grün", "ressourcenschutz", "verantwort", "ensorg"],
    "Transparency":     ["künstlische Intelligenz", "KI", "speicher", "cookie", "sicherheit", "datenschutz",
                        "verarbeite", "lösch", "business modell", "registrier", "zertifikat", "social media",
                        "medien", "youtube", "twitter", "facebook"],
    "Financial":        ["preis", "zahlung", "trial", "finanz", "abrechn", "bonus"],
    "Security":         ["zertifikat", "haftung", "garantie", "gewährleist", "erstatt", "widerruf","vergüt"],
    "Privacy":          ["datenschutz", "dsgvo", "cookie", "ip-adress", "track", "personenbezog", "lösch",
                        "sicherheit", "speicher"],
    "Compensation":     ["vergüt", "kontakt", "mail", "telefon", "fax", "tel", "register", "vorstand",
                        "kontakt", "umsatzsteuer", "geschäftführ", "beauftragt", "verantwortlich",
                        "vorsitzend", "vertreten", "aufsichtsrat"],    
    "Participation":   ["beteilig", "newsletter", "abonieren", "umfrage", "Interessengrup"],   
}



def rate_cdr(language, cdr_dims, text):
    ratings = {}
    if  language=="EN":
        for key in cdr_dims:                
            ref_vector = np.ones((len(buzzwords_en[key]), ), dtype=np.float16)
            is_available = []
            for word in buzzwords_en[key]:
                if re.search(word + r"\w*", text.lower()):
                    is_available.append(1.)
                else:
                    is_available.append(0.)
            doc_vector = np.array(is_available)
            accuracy = accuracy_score(ref_vector, doc_vector)
 
            if accuracy >= 0.60:
                ratings[key] = 5
            elif accuracy >= 0.30 and accuracy < 0.60:
                ratings[key] = 3
            else:
                ratings[key] = 1
        for key in buzzwords_en.keys():
            if key not in cdr_dims:
                ratings[key] = 1
        return ratings
    elif language=="DE":
        for key in cdr_dims:                
            ref_vector = np.ones((len(buzzwords_de[key]), ), dtype=np.float16)
            is_available = []
            for word in buzzwords_de[key]:
                if re.search(word + r"\w*", text.lower()):
                    is_available.append(1.)
                else:
                    is_available.append(0.)
            doc_vector = np.array(is_available)
            accuracy = accuracy_score(ref_vector, doc_vector)

            if accuracy >= 0.60:
                ratings[key] = 5
            elif accuracy >= 0.30 and accuracy < 0.60:
                ratings[key] = 3
            else:
                ratings[key] = 1

        for key in buzzwords_en.keys():
            if key not in cdr_dims:
                ratings[key] = 1
        return ratings
    else:
        return None


