"""
    CDR-CAT-Project Prototype:
    - load *.csv file (dataset where each data instance has a list of tokens and a label)
    - vectorization of tokens and label Encoding
    - plotting of 'confusion matrix'
    - model evaluation
"""

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
import numpy as np
import dill
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ast import literal_eval



def load_data(file_path):
    """
        - loads *.csv file containing dataset
        - each dataset contains list of tokens, label
    """
    try:
        df = pd.read_csv(file_path, encoding="utf-8")
        X_data = df['data'].apply(literal_eval).tolist()
        labels = df['label']
        return X_data, labels

    except FileNotFoundError as e:
        print(e)
    except Exception as e:
        print(e)
    

def vectorize_data(X_data, labels):
    """"
        - input: list of tokens, labels
        - output: array of features, encoded labels, fitted vectorizer and encoder
    """
    vectorizer = CountVectorizer(strip_accents=None,
                            lowercase=False,
                            tokenizer = lambda x:x,
                            preprocessor = lambda x:x,
                            decode_error='ignore')
    X = vectorizer.fit_transform(X_data).toarray()
    # Label Encoding 
    label_encoder = LabelEncoder()
    label_encoder.fit(['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content'])
    y = label_encoder.transform(labels)
    return X, y, vectorizer, label_encoder


def compute_plot_cm(y_true, y_pred, img_name):
    """
        plots confusion matrix
        input: true value, predicted value, name of image generated after plotting
        output: image is saved
    """
    matrix = confusion_matrix(y_true, y_pred)
    labels = ['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
    fig, ax = plt.subplots(figsize=(15,14))
    _ = ax.imshow(matrix, cmap='Oranges')
    _ = ax.set_xticks(np.arange(len(labels)))
    _ = ax.set_yticks(np.arange(len(labels)))
    _ = ax.set_xticklabels(labels, rotation=45)
    _ = ax.set_yticklabels(labels)
    _ = ax.set_xlabel('Predicted', fontsize=25)
    _ = ax.set_ylabel('True', fontsize=25)
    _ = ax.xaxis.set_label_position('top')
    ax.tick_params(top=True, 
               bottom=False,
               labeltop=True, 
               labelbottom=False,
               labelsize=15,)
    for i in range(len(labels)):
        for j in range(len(labels)):
            ax.text(j, i,
                matrix[i,j],
                ha='center',
                va='center',
                fontsize=25)
    ax.spines[:].set_visible(False)
    ax.set_xticks(np.arange(matrix.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(matrix.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)
    plt.savefig(f"./images/{img_name}.png")
    plt.show()


def split_data(clf, X, y):
    """
        splitting of data into training and test set
    """
    # Stratified Split of the datasets
    sss = StratifiedShuffleSplit(n_splits=10, test_size=0.05, random_state=42)   
    # Evaluate model on different stratified splits
    scores = cross_val_score(clf, X, y, cv=sss)
    train_indices, test_indices = list(sss.split(X, y))[np.argmax(scores)]
    # use the best split that has yieled best result
    X_train, y_train = X[train_indices], y[train_indices]
    X_test, y_test = X[test_indices], y[test_indices]
    # Fit the Model
    return X_train, X_test, y_train, y_test


def main():
    data_path = "./data/data_tokens_url_classification_en.csv"
    X_data, labels = load_data(data_path)
    # Vectorize datasets
    X, y, vectorizer, encoder = vectorize_data(X_data, labels)
    dill.dump(vectorizer, open("./models/vectorizer_en.pkl", "wb"))
    dill.dump(encoder, open("./models/label_encoder.pkl", "wb"))
    # Define Model
    # clf = RandomForestClassifier(n_estimators=100, random_state=45)
    clf = MultinomialNB()

    # Split dataset
    X_train, X_test, y_train, y_test = split_data(clf, X, y)
    # Fit the Model
    clf.fit(X_train, y_train)
    # save Model
    dill.dump(clf, open('./models/naive_bayes_model_en.pkl', 'wb'))

    # y_pred = clf.predict(X_test)
    # acc_score = accuracy_score(y_test, y_pred)
    # compute_plot_cm(y_test, y_pred, 'cm_naive_bayes_test_en')
    # print("Classification Report (Test set): ")
    # print(f"Accuracy: {acc_score}")
    # cls_report = classification_report(
    #                     y_test,
    #                     y_pred,
    #                     output_dict=False,
    #                     target_names=['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
    #                     )
    # print(cls_report)

    # y_pred = clf.predict(X_train)
    # acc_score = accuracy_score(y_train, y_pred)
    # compute_plot_cm(y_train, y_pred, 'cm_naive_bayes_train_en')
    # print("Classification Report (Train set): ")
    # print(f"Accuracy: {acc_score}")
    # cls_report = classification_report(
    #                     y_train,
    #                     y_pred,
    #                     output_dict=False,
    #                     target_names=['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
    #                     )
    # print(cls_report)


if __name__ == '__main__':
    main()
