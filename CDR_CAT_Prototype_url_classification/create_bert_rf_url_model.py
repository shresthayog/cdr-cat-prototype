"""
    CDR-CAT-Protype:                                                                    
    - creates Random Forest Model
    - Model is trained on features generated by pre-trained BERT Model
    - For german text, 'distilbert-base-german-cased' is used
    - For english text, 'distilbert-base-cased' is used
    - set NUM_CHUNK to 4 (output dimension of BERT model is 4*512)
"""

from tqdm import tqdm
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, accuracy_score, classification_report
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.model_selection import cross_val_score
from transformers import DistilBertModel, DistilBertTokenizer
import numpy as np
import dill
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch



def load_data(file_path):
    """ loads data into a dataframe """
    try:
        df = pd.read_csv(file_path, encoding="utf-8")
        return df
    except FileNotFoundError as e:
        print(e)
    except Exception as e:
        print(e)


def compute_plot_cm(y_true, y_pred, img_name):
    """
        plots confusion matrix
        input: true value, predicted value, name of image generated after plotting
        output: image is saved
    """
    matrix = confusion_matrix(y_true, y_pred)
    labels = ['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
    fig, ax = plt.subplots(figsize=(15,14))
    _ = ax.imshow(matrix, cmap='Oranges')
    _ = ax.set_xticks(np.arange(len(labels)))
    _ = ax.set_yticks(np.arange(len(labels)))
    _ = ax.set_xticklabels(labels, rotation=45)
    _ = ax.set_yticklabels(labels)
    _ = ax.set_xlabel('Predicted', fontsize=25)
    _ = ax.set_ylabel('True', fontsize=25)
    _ = ax.xaxis.set_label_position('top')
    ax.tick_params(top=True, 
               bottom=False,
               labeltop=True, 
               labelbottom=False,
               labelsize=15,)
    for i in range(len(labels)):
        for j in range(len(labels)):
            ax.text(j, i,
                matrix[i,j],
                ha='center',
                va='center',
                fontsize=25)
    ax.spines[:].set_visible(False)
    ax.set_xticks(np.arange(matrix.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(matrix.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)
    plt.savefig(f"./images/{img_name}.png")
    plt.show()

def split_data(clf, X, y):
    """
        splitting of data into training and test set
    """
    # Stratified Split of the datasets
    sss = StratifiedShuffleSplit(n_splits=10, test_size=0.05, random_state=42)   
    # Evaluate model on different stratified splits
    scores = cross_val_score(clf, X, y, cv=sss)
    train_indices, test_indices = list(sss.split(X, y))[np.argmax(scores)]
    # use the best split that has yieled best result
    X_train, y_train = X[train_indices], y[train_indices]
    X_test, y_test = X[test_indices], y[test_indices]
    # Fit the Model
    return X_train, X_test, y_train, y_test

def label_encode(df):
    """" encoding of labels """
    label_encoder = LabelEncoder()
    label_encoder.fit(['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content'])
    labels = label_encoder.transform(df['label'])
    return labels, label_encoder

def generate_features_bert(df):
    """
    German: distilbert-base-german-cased
    Englsih: distilbert-base-uncased
    """
    tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-german-cased')
    model = DistilBertModel.from_pretrained('distilbert-base-german-cased')
    tokenized_series = df['data'].apply((lambda x: tokenizer.encode(x, 
                                                        add_special_tokens=True, 
                                                        truncation=False,)))
    NUM_CHUNK = 4
    features_list = []
    for tokenized in tqdm(tokenized_series, desc="Progress"):
        tokenized = torch.tensor(tokenized)
        # split tensor in max length of 512
        input_id_chunks = tokenized.split(512)
        mask_chunks = tokenized.split(512)
        # create zero tensors
        input_ids = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
        attention_mask = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
        # number of splits
        len_chunk = len(input_id_chunks)

        if len_chunk > NUM_CHUNK:
            for i in range(NUM_CHUNK):
                input_ids[i] = input_id_chunks[i]
                attention_mask[i] = mask_chunks[i]
        else:
            for i in range(len_chunk):
                pad_len = 512 - input_id_chunks[i].shape[0]
                if pad_len > 0:
                    input_ids[i] = torch.cat([input_id_chunks[i], torch.Tensor([0] * pad_len)])
                    attention_mask[i] = torch.cat([mask_chunks[i], torch.Tensor([0] * pad_len)])
                else: 
                    input_ids[i] = input_id_chunks[i]
                    attention_mask[i] = mask_chunks[i]
        with torch.no_grad():
            last_hidden_states = model(input_ids, attention_mask=attention_mask)
        features = torch.reshape(last_hidden_states[0][:,0,:], (-1,)).numpy()
        where_are_NaNs = np.isnan(features)
        features[where_are_NaNs] = 0  
        features_list.append(features)             
    return np.stack( features_list, axis=0 )


def main():
    data_path = "./data/data_bert_url_classification_de.csv"
    df = load_data(data_path)

    features = generate_features_bert(df)

    labels, encoder = label_encode(df)
    dill.dump(encoder, open("./models/multilabel_encoder.sav", "wb"))

    clf = RandomForestClassifier()
    # Split dataset
    X_train, X_test, y_train, y_test = split_data(clf, features, labels)

    # Fit the Model
    clf.fit(X_train, y_train)
    dill.dump(clf, open('./models/bert_rf_model_de.sav', 'wb'))

    y_pred = clf.predict(X_test)
    acc_score = accuracy_score(y_test, y_pred)
    compute_plot_cm(y_test, y_pred, 'cm_bert_rf_test_de')
    print("Classification Report (Test set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_test,
                        y_pred,
                        output_dict=False,
                        target_names=['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
                        )
    print(cls_report)

    y_pred = clf.predict(X_train)
    acc_score = accuracy_score(y_train, y_pred)
    compute_plot_cm(y_train, y_pred, 'cm_bert_rf_test_de')
    print("Classification Report (Test set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_train,
                        y_pred,
                        output_dict=False,
                        target_names=['ToC', 'Data Privacy', 'Imprint', 'Self Imposed', 'Content']
                        )
    print(cls_report)
    

if __name__ == '__main__':
    main()
