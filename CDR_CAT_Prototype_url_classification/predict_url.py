
"""
    Project: CDR-CAT-Prototype
    Prediction of URL category
"""

import utils
import dill
import os
import fnmatch
import spacy
from spacy.language import Language
from spacy_langdetect import LanguageDetector


# load all models, vectorizers and encoders
with open("./models/label_encoder.sav", "rb") as encoder:
    label_encoder = dill.load(encoder)
# =============== For German Text ============
de_nlp = spacy.load('./models/de_core_news_lg/de_core_news_md-3.1.0')
with open("./models/naive_bayes_model_de.sav", "rb") as model:
    de_model = dill.load(model)
# with open("./models/random_forest_model_de.sav", "rb") as model:
#     de_model = dill.load(model)
with open("./models/vectorizer_de.sav", "rb") as vectorizer:
    de_vectorizer = dill.load(vectorizer)

# ============== For English text ============
en_nlp = spacy.load("./models/en_core_web_lg/en_core_web_md-3.1.0")
with open("./models/naive_bayes_model_en.sav", "rb") as model:
    en_model = dill.load(model)
# with open("./models/random_forest_model_en.sav", "rb") as model:
#     en_model = dill.load(model)
with open("./models/vectorizer_en.sav", "rb") as vectorizer:
    en_vectorizer = dill.load(vectorizer)



def predict_lang(text):
    """ 
    To predict the language of the given text: 
    """
    def get_lang_detector(nlp, name):
        return LanguageDetector()

    nlp = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
    Language.factory("language_detector", func=get_lang_detector)
    nlp.add_pipe('language_detector', last=True)
    doc = nlp(text)
    pred_language = doc._.language['language']
    if pred_language == "en":
        return 'EN'
    elif pred_language == "de":
        return 'DE'
    return 'UNKNOWN'


def predict_dir(directory_name):
    """ 
    input : directory containing *.txt files
    outputs : list of tuples [(file_name, language, prediction), ..........]
    """
    # list of tuples
    results = list()
    try:
        for file_name in os.listdir(directory_name+'/'):
            if fnmatch.fnmatch(file_name, '*.txt'):
                with open(directory_name+"/"+file_name, 'r', encoding='utf-8') as f:
                    text = f.read()
                language = predict_lang(text)
                if language == 'DE':
                    processed_text = utils.process_text(de_nlp, text)
                    x = de_vectorizer.transform([processed_text]).toarray()
                    y = de_model.predict(x)
                    pred_class = label_encoder.inverse_transform(y)[0]
                    results.append((file_name, language, pred_class))   
                elif language == 'EN':
                    processed_text = utils.process_text(en_nlp, text)
                    x = en_vectorizer.transform([processed_text]).toarray()
                    y = en_model.predict(x)
                    pred_class = label_encoder.inverse_transform(y)[0]
                    results.append((file_name, language, pred_class))
                else:
                    results.append((file_name, language, 'NO_PREDICTION'))
    except FileNotFoundError as e:
        print(e)
        print("Provide a valid path...")
    except Exception as e:
        print(e)

    return results


def predict_path_list(path_lists):
    """
        input: string(path)
        outputs: (file_name, predictions)
                
    """
    predictions = []
    try:
        for path_name in path_lists:
            with open(path_name, "r", encoding="utf-8") as f:
                text = f.read()
            language = predict_lang(text)
            if language == 'DE':
                processed_text = utils.process_text(de_nlp, text)
                x = de_vectorizer.transform([processed_text]).toarray()
                y = de_model.predict(x)
                pred_class = label_encoder.inverse_transform(y)[0]
            elif language == 'EN':
                processed_text = utils.process_text(en_nlp, text)
                x = en_vectorizer.transform([processed_text]).toarray()
                y = en_model.predict(x)
                pred_class = label_encoder.inverse_transform(y)[0]
            else:
                pred_class = 'NO_PREDICTION'
            predictions.append((path_name, language, pred_class))
    except FileNotFoundError as e:
        print(e)
    except Exception as e:
        print(e)

    return predictions



def predict_text(text):
    """
    Categorize a single text
    """
    try:
        language = predict_lang(text)
        if language == 'DE':
            processed_text = utils.process_text(de_nlp, text)
            x = de_vectorizer.transform([processed_text]).toarray()
            y = de_model.predict(x)
            pred_class = label_encoder.inverse_transform(y)[0]
        elif language == 'EN':
            processed_text = utils.process_text(en_nlp, text)
            x = en_vectorizer.transform([processed_text]).toarray()
            y = en_model.predict(x)
            pred_class = label_encoder.inverse_transform(y)[0]
        else:
            pred_class = 'NO_PREDICTION'
        return language, pred_class

    except Exception as e:
        print(e)
    

