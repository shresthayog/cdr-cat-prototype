"""
    preprocessing of raw text for classical models:
    - Random Forest Classifier
    - Naive Bayes Classifier
"""

import pandas as pd
from tqdm import tqdm
import spacy
import utils

def main():
    #========= Processing of german datasets ==========
    nlp = spacy.load('de_core_news_lg')
    df = pd.read_json("./data/company_service_de.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text preprocessing...'):
        df.loc[i, 'data'], df.loc[i, 'label'] = utils.process_text(nlp, df.loc[i, 'data']), df.loc[i, 'label'][0]
    df.to_csv("./data/data_tokens_de.csv", index=False)
    #=========== Processing of english datasets ==========
    nlp = spacy.load('en_core_web_lg')
    df = pd.read_json("./data/company_service_en.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text preprocessing...'):
        df.loc[i, 'data'], df.loc[i, 'label'] = utils.process_text(nlp, df.loc[i, 'data']), df.loc[i, 'label'][0]
    df.to_csv("./data/data_tokens_en.csv", index=False)

if __name__ == '__main__':
    main()