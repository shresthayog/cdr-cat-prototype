URL Prediction (german), Naive Bayes:
=====================================
Classification Report (Test set): 
Accuracy: 0.9428571428571428
              precision    recall  f1-score   support

         ToC       1.00      0.80      0.89        10
Data Privacy       1.00      1.00      1.00         6
     Imprint       0.75      1.00      0.86         6
Self Imposed       1.00      1.00      1.00         8
     Content       1.00      1.00      1.00         5

    accuracy                           0.94        35
   macro avg       0.95      0.96      0.95        35
weighted avg       0.96      0.94      0.94        35

Classification Report (Train set): 
Accuracy: 0.9262672811059908
              precision    recall  f1-score   support

         ToC       0.93      0.92      0.92       177
Data Privacy       0.87      0.98      0.92       115
     Imprint       0.97      0.89      0.93       120
Self Imposed       0.94      0.92      0.93       157
     Content       0.93      0.91      0.92        82

    accuracy                           0.93       651
   macro avg       0.93      0.93      0.93       651
weighted avg       0.93      0.93      0.93       651


URL Prediction (english), Naive Bayes:
======================================
Classification Report (Train set): 
Accuracy: 1.0
              precision    recall  f1-score   support

         ToC       1.00      1.00      1.00         4
Data Privacy       1.00      1.00      1.00         2
     Imprint       1.00      1.00      1.00         2
Self Imposed       1.00      1.00      1.00         5
     Content       1.00      1.00      1.00         1

    accuracy                           1.00        14
   macro avg       1.00      1.00      1.00        14
weighted avg       1.00      1.00      1.00        14

Classification Report (Test set): 
Accuracy: 0.9416342412451362
              precision    recall  f1-score   support

         ToC       0.95      0.92      0.94        66
Data Privacy       0.87      1.00      0.93        41
     Imprint       0.95      0.93      0.94        41
Self Imposed       0.98      0.94      0.96        84
     Content       0.92      0.92      0.92        25

    accuracy                           0.94       257
   macro avg       0.93      0.94      0.94       257
weighted avg       0.94      0.94      0.94       257


URL Prediction (german), Random Forest:
======================================
Classification Report (Test set): 
Accuracy: 0.9428571428571428
              precision    recall  f1-score   support

         ToC       0.91      1.00      0.95        10
Data Privacy       1.00      0.83      0.91         6
     Imprint       0.86      1.00      0.92         6
Self Imposed       1.00      1.00      1.00         8
     Content       1.00      0.80      0.89         5

    accuracy                           0.94        35
   macro avg       0.95      0.93      0.93        35
weighted avg       0.95      0.94      0.94        35

Classification Report (Train set): 
Accuracy: 0.9984639016897081
              precision    recall  f1-score   support

         ToC       1.00      1.00      1.00       177
Data Privacy       0.99      1.00      1.00       115
     Imprint       1.00      1.00      1.00       120
Self Imposed       1.00      1.00      1.00       157
     Content       1.00      0.99      0.99        82

    accuracy                           1.00       651
   macro avg       1.00      1.00      1.00       651
weighted avg       1.00      1.00      1.00       651



URL Prediction (english), Random Forest:
======================================
Classification Report (Test set): 
Accuracy: 0.9285714285714286
              precision    recall  f1-score   support

         ToC       1.00      0.75      0.86         4
Data Privacy       1.00      1.00      1.00         2
     Imprint       1.00      1.00      1.00         2
Self Imposed       0.83      1.00      0.91         5
     Content       1.00      1.00      1.00         1

    accuracy                           0.93        14
   macro avg       0.97      0.95      0.95        14
weighted avg       0.94      0.93      0.93        14

Classification Report (Train set): 
Accuracy: 1.0
              precision    recall  f1-score   support

         ToC       1.00      1.00      1.00        66
Data Privacy       1.00      1.00      1.00        41
     Imprint       1.00      1.00      1.00        41
Self Imposed       1.00      1.00      1.00        84
     Content       1.00      1.00      1.00        25

    accuracy                           1.00       257
   macro avg       1.00      1.00      1.00       257
weighted avg       1.00      1.00      1.00       257




Random Forest and BERT Fine-tuning(german)
==========================================
Classification Report (Test set): 
Accuracy: 0.8857142857142857
              precision    recall  f1-score   support

         ToC       1.00      0.90      0.95        10
Data Privacy       0.83      0.83      0.83         6
     Imprint       0.67      1.00      0.80         6
Self Imposed       1.00      1.00      1.00         8
     Content       1.00      0.60      0.75         5

    accuracy                           0.89        35
   macro avg       0.90      0.87      0.87        35
weighted avg       0.91      0.89      0.89        35

Classification Report (Train set): 
Accuracy: 0.9953917050691244
              precision    recall  f1-score   support

         ToC       0.99      1.00      0.99       177
Data Privacy       0.99      1.00      1.00       115
     Imprint       1.00      1.00      1.00       120
Self Imposed       1.00      0.99      0.99       157
     Content       1.00      0.99      0.99        82

    accuracy                           1.00       651
   macro avg       1.00      1.00      1.00       651
weighted avg       1.00      1.00      1.00       651

Random Forest and BERT Fine-tuning(english)
==========================================
Classification Report (Test set): 
Accuracy: 0.9285714285714286
              precision    recall  f1-score   support
         ToC       1.00      1.00      1.00         4
Data Privacy       0.67      1.00      0.80         2
     Imprint       1.00      1.00      1.00         2
Self Imposed       1.00      1.00      1.00         5
     Content       0.00      0.00      0.00         1

    accuracy                           0.93        14
   macro avg       0.73      0.80      0.76        14
weighted avg       0.88      0.93      0.90        14

Classification Report (Train set): 
Accuracy: 0.9961089494163424
              precision    recall  f1-score   support

         ToC       0.99      1.00      0.99        66
Data Privacy       1.00      1.00      1.00        41
     Imprint       1.00      1.00      1.00        41
Self Imposed       1.00      0.99      0.99        84
     Content       1.00      1.00      1.00        25

    accuracy                           1.00       257
   macro avg       1.00      1.00      1.00       257
weighted avg       1.00      1.00      1.00       257