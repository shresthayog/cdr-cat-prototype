"""
    preprocessing of raw text for pre-trained BERT Model
"""

from tqdm import tqdm
import pandas as pd
import utils




def main():
    #============= Processing of german datasets ============
    df = pd.read_json("./data/company_service_de_all_2.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text preprocessing...'):
        df.loc[i, 'data'], df.loc[i, 'label'] = utils.process_text_bert(df.loc[i, 'data']), df.loc[i, 'label'][0]
    df.to_csv("./data/data_bert_url_classification_de.csv", index=False)
    #============ Processing of english datasets ==============
    # df = pd.read_json("./data/company_service_en_all_2.json")
    # df = df[['data', 'label']]
    # for i in tqdm(range(len(df)), desc='Text preprocessing...'):
    #     df.loc[i, 'data'], df.loc[i, 'label'] = utils.process_text_bert(df.loc[i, 'data']), df.loc[i, 'label'][0]
    # df.to_csv("./data/data_bert_url_classification_en.csv", index=False)


if __name__ == '__main__':
    main()