"""
    CDT-CAT-Prototype
    - Multilabel classification model by fine-tuning pre-trained BERT model
        - 8 labels(CDR dimensions)
        - Random Forest Classifier
        - set number of chunks of length 512 for BERT input (BERT outputs are then concatenated)
        - generate classification report
"""

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from skmultilearn.problem_transform import BinaryRelevance
import numpy as np
import dill
import numpy as np
import pandas as pd
from transformers import DistilBertModel, DistilBertTokenizer
import torch
from tqdm import tqdm
from ast import literal_eval


def load_data(file_path):
    df = pd.read_csv(file_path, encoding="utf-8")
    return df

def label_encode(df):
    label_encoder = MultiLabelBinarizer()
    label_encoder.fit([["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]])
    labels = label_encoder.transform(df['label'].apply(literal_eval).tolist())
    return labels, label_encoder

def generate_features_bert(df):
    """
    German: distilbert-base-german-cased
    Englsih: distilbert-base-uncased
    """
    tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-cased')
    model = DistilBertModel.from_pretrained('distilbert-base-cased')
    tokenized_series = df['data'].apply((lambda x: tokenizer.encode(x, 
                                                        add_special_tokens=True, 
                                                        truncation=False,)))
    NUM_CHUNK = 6
    features_list = []
    for tokenized in tqdm(tokenized_series, desc="Progress"):
        tokenized = torch.tensor(tokenized)
        # split tensor in max length of 512
        input_id_chunks = tokenized.split(512)
        mask_chunks = tokenized.split(512)
        # create zero tensors
        input_ids = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
        attention_mask = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
        # number of splits
        len_chunk = len(input_id_chunks)

        if len_chunk > NUM_CHUNK:
            for i in range(NUM_CHUNK):
                input_ids[i] = input_id_chunks[i]
                attention_mask[i] = mask_chunks[i]
        else:
            for i in range(len_chunk):
                pad_len = 512 - input_id_chunks[i].shape[0]
                if pad_len > 0:
                    input_ids[i] = torch.cat([input_id_chunks[i], torch.Tensor([0] * pad_len)])
                    attention_mask[i] = torch.cat([mask_chunks[i], torch.Tensor([0] * pad_len)])
                else: 
                    input_ids[i] = input_id_chunks[i]
                    attention_mask[i] = mask_chunks[i]
        with torch.no_grad():
            last_hidden_states = model(input_ids, attention_mask=attention_mask)
        features = torch.reshape(last_hidden_states[0][:,0,:], (-1,)).numpy()
        where_are_NaNs = np.isnan(features)
        features[where_are_NaNs] = 0  
        features_list.append(features)             
    return np.stack( features_list, axis=0 )



def main():
    data_path = "./data/data_multilabel_bert_en.csv"
    df = load_data(data_path)
    features = generate_features_bert(df)
    labels, encoder = label_encode(df)
    # save the encoder
    dill.dump(encoder, open("./models/multilabel_encoder.sav", "wb"))
    # define multilabel classifier
    clf = BinaryRelevance(RandomForestClassifier())
    # # Split dataset   
    X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.1, random_state=44) #en: 44, 9869

    # Fit the Model
    clf.fit(X_train, y_train)
    dill.dump(clf, open('./models/multilabel_bert_rf_model_6_512_en.sav', 'wb'))

    y_pred = clf.predict(X_test)
    acc_score = accuracy_score(y_test, y_pred)
    print("Classification Report (Test set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_test,
                        y_pred,
                        output_dict=False,
                        target_names=["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]
                        )
    print(cls_report)

    y_pred = clf.predict(X_train)
    acc_score = accuracy_score(y_train, y_pred)
    print("Classification Report (Train set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_train,
                        y_pred,
                        output_dict=False,
                        target_names=["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]
                        )
    print(cls_report)
    

if __name__ == '__main__':
    main()




