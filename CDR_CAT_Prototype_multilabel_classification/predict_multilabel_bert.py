"""
    CDR-CAT-Prototype
    - Multilabel Prediction of CDR dimensions
"""

import os
import fnmatch
import dill
from transformers import DistilBertTokenizer
from transformers import DistilBertModel
import torch
import numpy as np
import spacy
from spacy.language import Language
from spacy_langdetect import LanguageDetector
import utils


# number of chunks each with 512 tokens
NUM_CHUNK = 6
# load all models, vectorizers and encoders
with open("./models/multilabel_encoder.sav", "rb") as encoder:
    multilabel_encoder = dill.load(encoder)
# for classifiying german doc
with open("./models/multilabel_bert_rf_model_6_512_de.sav", "rb") as model:
    de_model = dill.load(model)
# for classifying english doc
with open("./models/multilabel_bert_rf_model_6_512_en.sav", "rb") as model:
    en_model = dill.load(model)


def predict_lang(text):
    """ 
    To predict the language of the given text: 
    """
    def get_lang_detector(nlp, name):
        return LanguageDetector()

    nlp = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
    Language.factory("language_detector", func=get_lang_detector)
    nlp.add_pipe('language_detector', last=True)
    doc = nlp(text)
    pred_language = doc._.language['language']
    if pred_language == "en":
        return 'EN'
    elif pred_language == "de":
        return 'DE'
    return 'UNKNOWN'

def generate_features_bert(language, text_features):
    """
    - Generates the representation of 'text_features'
    """
    if language == "EN":
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = DistilBertModel.from_pretrained('distilbert-base-uncased')
    else:
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-german-cased')
        model = DistilBertModel.from_pretrained('distilbert-base-german-cased')
    tokenized = tokenizer.encode(text_features,
                                add_special_tokens=True,
                                truncation=False,)
    tokenized = torch.tensor(tokenized)
    # split tensor in max length of 512
    input_id_chunks = tokenized.split(512)
    mask_chunks = tokenized.split(512)
    # create zero tensors
    input_ids = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
    attention_mask = torch.zeros(NUM_CHUNK,512, dtype=torch.int64)
    # number of splits
    len_chunk = len(input_id_chunks)

    if len_chunk > NUM_CHUNK:
        for i in range(NUM_CHUNK):
            input_ids[i] = input_id_chunks[i]
            attention_mask[i] = mask_chunks[i]
    else:
        for i in range(len_chunk):
            pad_len = 512 - input_id_chunks[i].shape[0]
            if pad_len > 0:
                input_ids[i] = torch.cat([input_id_chunks[i], torch.Tensor([0] * pad_len)])
                attention_mask[i] = torch.cat([mask_chunks[i], torch.Tensor([0] * pad_len)])
            else:
                input_ids[i] = input_id_chunks[i]
                attention_mask[i] = mask_chunks[i]
    # feature generation, reshaping, substituting 'nan' with 0
    with torch.no_grad():
        last_hidden_states = model(input_ids, attention_mask=attention_mask)
    features = torch.reshape(last_hidden_states[0][:,0,:], (-1,)).numpy()
    where_are_nans = np.isnan(features)
    features[where_are_nans] = 0
    return features

def predict_dir(directory_name):
    """
    input : directory containing *.txt files
    outputs : list of tuples [(file_name, language, prediction), ..........]
    """
    # list of tuples
    results = []
    for file_name in os.listdir(directory_name+'/'):
        if fnmatch.fnmatch(file_name, '*.txt'):
            with open(directory_name+"/"+file_name, 'r', encoding='utf-8') as file:
                text = file.read()
            language = predict_lang(text)
            if language != 'UNKNOWN':
                processed_text = utils.process_text_bert(text)
                bert_out = generate_features_bert(language, processed_text)
                if language == 'EN':
                    model_out = en_model.predict(bert_out[np.newaxis,:])
                    pred_class = multilabel_encoder.inverse_transform(model_out)[0]
                else:
                    model_out = de_model.predict(bert_out[np.newaxis,:])
                    pred_class = multilabel_encoder.inverse_transform(model_out)[0]
                results.append((file_name, language, pred_class))
            else:
                results.append((file_name, language, 'NO_PREDICTION'))
    return results

def predict_path_list(path_lists):
    """
        input: string
        outputs: (file_name, predictions)
    """
    predictions = []
    for path_name in path_lists:
        with open(path_name, "r", encoding="utf-8") as file:
            text = file.read()
        language = predict_lang(text)
        if language != 'UNKNOWN':
            processed_text = utils.process_text_bert(text)
            bert_out = generate_features_bert(language, processed_text)
            if language == 'EN':
                model_out = en_model.predict(bert_out[np.newaxis,:])
                pred_class = multilabel_encoder.inverse_transform(model_out)[0]
            else:
                model_out = de_model.predict(bert_out[np.newaxis,:])
                pred_class = multilabel_encoder.inverse_transform(model_out)[0]
        else:
            pred_class = 'NO_PREDICTION'
        predictions.append((path_name, language, pred_class))
    return predictions

def predict_text(text):
    """
    - Predict the CDR Dimension of a single text
    """
    language = predict_lang(text)
    if language != 'UNKNOWN':
        processed_text = utils.process_text_bert(text)
        bert_out = generate_features_bert(language, processed_text)
        if language == 'EN':
            model_out = en_model.predict(bert_out[np.newaxis,:])
            pred_class = multilabel_encoder.inverse_transform(model_out)[0]
        else:
            model_out = de_model.predict(bert_out[np.newaxis,:])
            pred_class = multilabel_encoder.inverse_transform(model_out)[0]
    else:
        pred_class = 'NO_PREDICTION'
    return language, pred_class


