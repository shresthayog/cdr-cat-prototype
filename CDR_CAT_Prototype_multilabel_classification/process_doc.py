"""
    CDR-CAT-Prototype:
    - Processing of raw datasets
    - saves dataset in '*.csv'
    - First columns contains list of tokens and second column contains corresponding label
"""

import spacy
import utils
import pandas as pd
from tqdm import tqdm


def main():
    # ====== Processing of german datasets ======
    nlp = spacy.load('de_core_news_lg')
    df = pd.read_json("./data/CDR_multilabel_dataset_de.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text(de) preprocessing...'):
        df.loc[i, 'data'] = utils.process_text(nlp, df.loc[i, 'data'])
    df.to_csv("./data/data_tokens_multilabel_de.csv", index=False)
    
    # ====== Processing of english datasets =======
    nlp = spacy.load('en_core_web_lg')
    df = pd.read_json("./data/CDR_multilabel_dataset_en.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text(en) preprocessing...'):
        df.loc[i, 'data'] = utils.process_text(nlp, df.loc[i, 'data'])
    df.to_csv("./data/data_tokens_multilabel_en.csv", index=False)



if __name__ == '__main__':
    main()