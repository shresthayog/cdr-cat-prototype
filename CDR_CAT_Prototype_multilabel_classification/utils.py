"""
    CDR-CAT-Prototype
    - processing of raw text
"""

import re
import unicodedata
import string
import geotext

ALL_LETTERS = string.ascii_letters

def remove_link(text):
    """ removes URLs """
    pattern = r"(http|https)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?"
    text = re.sub(pattern, '', text)
    pattern = r"www.[a-zA-Z0-9-]+.[a-zA-Z0-9-]*.[a-z]+"
    text = re.sub(pattern, '', text)
    pattern = r"[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+\.[a-z]+"
    text = re.sub(pattern, '', text)
    return text


def remove_puncts(text):
    """ removes punctuations """
    pattern = r"[\[\]()-]"
    text = re.sub(pattern, '', text)
    pattern = r"[#=&*%:\\]"
    text = re.sub(pattern, '', text)
    pattern = r"[§○●■€$]"
    text = re.sub(pattern, '', text)
    return text

def replace_words(text):
    """  replaces the telephone number, fax number, street name, email with unique tokens """
    pattern = r"\b[a-zA-Z-]+[Ss][tT][rR][aA][ß]?[sS]?[sS]?[eE]\b"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[a-zA-Z-]+[Ss][Tt][Rr]\.\s"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[a-zA-Z-]+[\s]?[Ww][Ee][Gg]\b"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[a-zA-Z-]+[\s-][Aa][Ll][Ll][Ee][Ee]\b"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[aA][mMnN][\s]?[a-zA-Z]+[\s]?[a-zA-Z]*\b"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[A-Za-z-]*[\s-]?[Pp][Ll][Aa][Tt][Zz]?\b"
    text = re.sub(pattern, "streetname", text)
    pattern = r"\b[A-Za-z0-9-_\.]+[@]{1}[A-Za-z0-9]+[\.]{1}[a-zA-Z]+\b"
    text = re.sub(pattern, 'emailaddress', text)
    pattern = r"\b[+]?[\d]+[-/\s]{1}[\d]+[-/\s]+[\d]*[-/\s]*[\d]*[-/\s]*[\d]*[-/\s]*[\d]*[-/\s]*[\d]\b"
    text = re.sub(pattern, 'telfaxnumber', text)
    pattern = r"\b[0-9]{5}\b"
    text = re.sub(pattern, 'postalcode', text)
    return text


def extract_names(nlp, text):
    """ identifies cities and personal name and replaces it with unique token."""
    doc = nlp(text)
    names = []
    for token in doc.ents:
        if(token.label_) == 'PER':
            names.append(str(token))
    for name in names:
        text = text.replace(name, 'personname')
    cities = geotext.GeoText(text).cities
    for city in cities:
        text = text.replace(city, 'cityname')
    return text

def unicode_to_ascii(s):
    """ normalize unicode data """
    return ''.join(
        c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn' and c in ALL_LETTERS
    )

def process_spacy_tokens(nlp, text): 
    """ selects only meaningful tokens """
    doc = nlp(text)
    new_tokens = []
    for token in doc:
        # print(token, token.pos_)
        if token.pos_ == 'SPACE' or token.pos_ == 'PUNCT':
            continue
        if token.pos_ == 'DET' or token.pos_ == 'CCONJ':
            continue
        if token.pos_ == 'ADP':
            continue
        if token.like_num == True:
            continue
        if token.pos_ == 'VERB':
            token = token.lemma_
            new_tokens.append(unicode_to_ascii(str(token).lower()))
            continue
        
        new_tokens.append(unicode_to_ascii(str(token).lower()))
    return new_tokens

def remove_stopwords(nlp, tokens):
    """ removes stopwords """
    stopwords = nlp.Defaults.stop_words
    for token in tokens:
        if token in stopwords:
            tokens.remove(token)
    return tokens

def remove_empty_string(token_ls):
    """ remvoes empty strings"""
    for token in token_ls:
        if len(token) == 0:
            token_ls.remove(token)
    return token_ls


def process_text(nlp, text):
    text = extract_names(nlp, text)
    text = remove_link(text)
    text = remove_puncts(text)
    text = replace_words(text)
    tokens = process_spacy_tokens(nlp, text)
    tokens = remove_stopwords(nlp, tokens)
    tokens = remove_empty_string(tokens)
    return tokens

def process_text_bert(text):
    text = remove_link(text)
    text = remove_puncts(text)
    text = text.replace('\n', ' ')
    return text