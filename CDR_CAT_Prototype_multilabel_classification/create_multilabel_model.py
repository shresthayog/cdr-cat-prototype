"""
    CDR-CAT-Prototype:
    - creates multilabel classifier for classifying CDR dimensions
"""

from sklearn.metrics import accuracy_score, classification_report
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import MultiLabelBinarizer
import numpy as np
import dill
import numpy as np
import pandas as pd
from ast import literal_eval
from skmultilearn.problem_transform import BinaryRelevance
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

def load_data(file_path):
    df = pd.read_csv(file_path, encoding="utf-8")
    X_data = df['data'].apply(literal_eval).tolist()
    labels = df['label'].apply(literal_eval).tolist()
    return X_data, labels

def vectorize_data(X_data, labels):
    vectorizer = CountVectorizer(strip_accents=None,
                            lowercase=False,
                            tokenizer = lambda x:x,
                            preprocessor = lambda x:x,
                            decode_error='ignore')
    X = vectorizer.fit_transform(X_data).toarray()
    # Label Encoding 
    mlb = MultiLabelBinarizer()
    mlb.fit([["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]])
    y = mlb.transform(labels)
    return X, y, vectorizer, mlb


def main():
    data_path = "./data/data_tokens_multilabel_de.csv"
    X_data, labels = load_data(data_path)
    # Vectorize datasets
    X, y, vectorizer, encoder = vectorize_data(X_data, labels)
    # save vectorizer and encoder
    dill.dump(vectorizer, open("./models/vectorizer_de.sav", "wb"))
    dill.dump(encoder, open("./models/multilabel_encoder.sav", "wb"))
    # clf = BinaryRelevance(MultinomialNB())
    clf = BinaryRelevance(RandomForestClassifier())
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.05, random_state=5670)# 42
    # Fit the Model
    clf.fit(X_train, y_train)
    # save Model
    dill.dump(clf, open('./models/multilabel_random_forest_model_de.sav', 'wb'))

    y_pred = clf.predict(X_test)
    acc_score = accuracy_score(y_test, y_pred)
    print("Classification Report (Test set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_test,
                        y_pred,
                        output_dict=False,
                        target_names=["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]
                        )
    print(cls_report)

    y_pred = clf.predict(X_train)
    acc_score = accuracy_score(y_train, y_pred)
    print("Classification Report (Train set): ")
    print(f"Accuracy: {acc_score}")
    cls_report = classification_report(
                        y_train,
                        y_pred,
                        output_dict=False,
                        target_names=["Access", "Awareness", "Transparency", "Financial", "Security", "Privacy", "Compensation", "Participation"]
                        )
    print(cls_report)
    

if __name__ == '__main__':
    main()



