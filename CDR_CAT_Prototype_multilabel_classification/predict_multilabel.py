"""
    CDR-CAT-Prototype: 
    - Prediction of CDR Dimensions (Multilabel)
"""

import os
import fnmatch
import spacy
import dill
from spacy.language import Language
from spacy_langdetect import LanguageDetector
import utils


# load all models, vectorizers and encoders
with open("./models/multilabel_encoder.sav", "rb") as encoder:
    multilabel_encoder = dill.load(encoder) 
#========== Models for german text ====================
de_nlp = spacy.load('./models/de_core_news_md/de_core_news_md-3.2.0')
with open("./models/multilabel_naive_bayes_model_de.sav", "rb") as model:
    de_model = dill.load(model)
# with open("./models/multilabel_random_forest_model_de.sav", "rb") as model:
#     de_model = dill.load(model)
with open("./models/vectorizer_de.sav", "rb") as vectorizer:
    de_vectorizer = dill.load(vectorizer)
#============ Models for english text ==================
en_nlp = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
with open("./models/multilabel_naive_bayes_model_en.sav", "rb") as model:
    en_model = dill.load(model)
# with open("./models/multilabel_random_forest_model_en.sav", "rb") as model:
#     de_model = dill.load(model)
with open("./models/vectorizer_en.sav", "rb") as vectorizer:
    en_vectorizer = dill.load(vectorizer)


def predict_lang(text):
    """ 
    To predict the language of the given text: 
    """
    def get_lang_detector(nlp, name):
        return LanguageDetector()

    nlp = spacy.load("./models/en_core_web_md/en_core_web_md-3.2.0")
    Language.factory("language_detector", func=get_lang_detector)
    nlp.add_pipe('language_detector', last=True)
    doc = nlp(text)
    pred_language = doc._.language['language']
    if pred_language == "en":
        return 'EN'
    elif pred_language == "de":
        return 'DE'
    return 'UNKNOWN'


def predict_dir(directory_name):
    """ 
    input : directory containing *.txt files
    outputs : list of tuples [(file_name, language, prediction), ..........]
    """
    # list of tuples
    results = list()
    for file_name in os.listdir(directory_name+'/'):
        if fnmatch.fnmatch(file_name, '*.txt'):
            with open(directory_name+"/"+file_name, 'r', encoding='utf-8') as f:
                text = f.read()
            language = predict_lang(text)
            if language == 'DE':
                processed_text = utils.process_text(de_nlp, text)
                x = de_vectorizer.transform([processed_text]).toarray()
                y = de_model.predict(x)
                pred_class = multilabel_encoder.inverse_transform(y)[0]
                results.append((file_name, language, pred_class))   
            elif language == 'EN':
                processed_text = utils.process_text(en_nlp, text)
                x = en_vectorizer.transform([processed_text]).toarray()
                y = en_model.predict(x)
                pred_class = multilabel_encoder.inverse_transform(y)[0]
                results.append((file_name, language, pred_class))
            else:
                results.append((file_name, language, 'UNKNOWN_LANGUAGE'))
    return results


def predict_path_list(path_lists):
    """
        input: string
        outputs: (file_name, predictions)
                
    """
    predictions = []
    for path_name in path_lists:
        with open(path_name, "r", encoding="utf-8") as f:
            text = f.read()
        language = predict_lang(text)
        if language == 'DE':
            processed_text = utils.process_text(de_nlp, text)
            x = de_vectorizer.transform([processed_text]).toarray()
            y = de_model.predict(x)
            pred_class = multilabel_encoder.inverse_transform(y)[0]
        elif language == 'EN':
            processed_text = utils.process_text(en_nlp, text)
            x = en_vectorizer.transform([processed_text]).toarray()
            y = en_model.predict(x)
            pred_class = multilabel_encoder.inverse_transform(y)[0]
        else:
             pred_class = 'UNKONWN_LANGUAGE'
        predictions.append((path_name, pred_class))
    return predictions



def predict_text(text):
    language = predict_lang(text)
    if language == 'DE':
        processed_text = utils.process_text(de_nlp, text)
        x = de_vectorizer.transform([processed_text]).toarray()
        y = de_model.predict(x)
        pred_class = multilabel_encoder.inverse_transform(y)[0]
    elif language == 'EN':
        processed_text = utils.process_text(en_nlp, text)
        x = en_vectorizer.transform([processed_text]).toarray()
        y = en_model.predict(x)
        pred_class = multilabel_encoder.inverse_transform(y)[0]
    else:
        pred_class = 'UNKNOWN_LANGUAGE'
    return pred_class




