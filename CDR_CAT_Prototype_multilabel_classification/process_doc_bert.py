import spacy
import utils
import pandas as pd
from tqdm import tqdm


def main():
    #========= Processing of german datasets ============
    df = pd.read_json("./data/CDR_multilabel_dataset_de.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text preprocessing...'):
        df.loc[i, 'data'] = utils.process_text_bert(df.loc[i, 'data'])
    df.to_csv("./data/data_multilabel_bert_de.csv", index=False)
    #=========== Processing of english datasets ===========

    df = pd.read_json("./data/CDR_multilabel_dataset_en.json")
    df = df[['data', 'label']]
    for i in tqdm(range(len(df)), desc='Text preprocessing...'):
        df.loc[i, 'data'] = utils.process_text_bert(df.loc[i, 'data'])
    df.to_csv("./data/data_multilabel_bert_en.csv", index=False)


if __name__ == "__main__":
    main()